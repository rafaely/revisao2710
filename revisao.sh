#!/bin/bash

if [ "$(whoami)" != "ifpb" ]; then
    echo "Este script deve ser executado como o usuário 'ifpb'."
    exit 1
fi

dir=${1:-"/home/ifpb"}

if [ ! -d "$dir" ]; then
    echo "O caminho especificado não é um diretório válido."
    exit 1
fi

files=$(find "$dir" -type f | wc -l)
dirs=$(find "$dir" -type d | wc -l)

echo "Arquivos: $files"
echo "Diretórios: $dirs"
 

